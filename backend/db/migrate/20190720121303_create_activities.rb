class CreateActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.string :name_en
      t.integer :category_id

      t.timestamps
    end
  end
end
