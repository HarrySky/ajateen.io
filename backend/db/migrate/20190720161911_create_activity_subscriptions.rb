class CreateActivitySubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :activity_subscriptions do |t|
      t.integer :soldier_id
      t.integer :activity_id

      t.timestamps
    end
  end
end
