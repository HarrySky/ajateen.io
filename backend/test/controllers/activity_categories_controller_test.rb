require 'test_helper'

class ActivityCategoriesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get activity_categories_index_url
    assert_response :success
  end

  test "should get show" do
    get activity_categories_show_url
    assert_response :success
  end

  test "should get update" do
    get activity_categories_update_url
    assert_response :success
  end

  test "should get new" do
    get activity_categories_new_url
    assert_response :success
  end

  test "should get destroy" do
    get activity_categories_destroy_url
    assert_response :success
  end

end
