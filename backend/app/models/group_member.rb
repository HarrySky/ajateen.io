class GroupMember < ApplicationRecord
  belongs_to :soldier
  belongs_to :group
end
