class Group < ApplicationRecord
  belongs_to :activity

  has_many :group_members
  has_many :soldiers, :through => :group_members

  def add_members members
    members.each do |soldier|
      GroupMembers.create(
        soldier: Soldier.find(soldier),
        group: self
      )
    end
  end
  def member_count
    soldiers.count
  end
end
