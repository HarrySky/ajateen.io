class ActivitiesController < ApplicationController
  def index
    @activities = Activity.all
    render :json => @activities
  end

  def show
    @activity = Activity.find(params[:id])
    return_detailed
  end

  def update
    @activity = Activity.find(params[:activity][:id])
    @activity.name_en = params[:activity][:name_en]
    @activity.category = ActivityCategory.find(params[:activity][:category_id])
    @activity.save

    return_detailed
  end

  def new
    @activity = Activity.create(
      name_en: params[:activity][:name_en],
      category: ActivityCategory.find(params[:activity][:category_id])
    )

    return_detailed
  end

  def destroy
    @activity = Activities.find(params[:id]).destroy
    render :json => @activity
  end

  private
  def return_detailed
    render :json => @activity, :include => [:category], :except => [:category_id]
  end
end
