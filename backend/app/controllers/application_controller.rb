class ApplicationController < ActionController::Base
    before_action :set_locale
    before_action :auth_token
    skip_before_action :verify_authenticity_token

    def auth_token
      @current_user = User.valid_token? params[:email], params[:token]

      unless @current_user
        head :unauthorized
      end
    end

    def set_locale
        I18n.locale = params[:locale] || I18n.default_locale
    end
end
