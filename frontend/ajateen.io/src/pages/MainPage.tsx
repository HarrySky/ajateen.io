import * as React from 'react';
import * as Reflux from 'reflux';
import './MainPage.css';
import { AddGroup } from '../components/AddGroup';
import { Bar } from '../components/Bar';
import { CategoriesMenu } from '../components/CategoriesMenu';
import { FeedTabs } from '../components/FeedTabs';
import { GroupDetails } from '../components/GroupDetails';
import { GroupsFeed } from '../components/GroupsFeed';
import { JoinedList } from '../components/JoinedList';
import { NewGroupDialog } from 'src/components/NewGroupDialog';

export class MainPage extends Reflux.Component {
  render() {
    return (
      <React.Fragment> 
        <Bar />
        <FeedTabs />
        <div className="feed_container">
          <GroupsFeed />
        </div>
        <AddGroup />
        <GroupDetails />
        <JoinedList />
        <CategoriesMenu />
        <NewGroupDialog />
      </React.Fragment>
    );
  }
}