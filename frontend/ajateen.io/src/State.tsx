import { createAction, Store } from 'reflux';

interface Category {
  id: number;
  name_en: string;
  created_at: string | Date;
  updated_at: string | Date;
}

interface Activity {
  id: number;
  name_en: string;
  category_id: number;
  created_at: string | Date;
  updated_at: string | Date;
}

interface Soldier {
  id: number;
  first_name: string;
  last_name: string;
  identity_code: number;
  rank_id: number;
  superior_id: Soldier;
  created_at: string | Date;
  updated_at: string | Date;
}

interface BasicGroupDetails {
  id: number;
  title: string;
  activity_id: number;
  participants: number;
  max_participants: number;
  start_time: string | Date;
  end_time: string | Date;
  location: string;
  created_at: string | Date;
  updated_at: string | Date;
}

interface FullGroupData {
  id: number;
  start_time: string | Date;
  end_time: string | Date;
  max_participants: number;
  location: string;
  created_at: string | Date;
  updated_at: string | Date;
  activity: Activity;
  soldiers: Soldier[];
}

interface AppState {
  categories: Category[];
  categoriesMenuAnchorEl?: HTMLElement;
  currentCategory?: Category;
  groups: BasicGroupDetails[];
  groupDetails?: FullGroupData;
  isJoinedListOpen: boolean;
  isAddingNewGroup: boolean;
}

export const fetchGroups = createAction('fetchGroups');

export const fetchGroupDetails = createAction('fetchGroupDetails');
export const closeGroupDetails = createAction('closeGroupDetails');

export const openJoinedList = createAction('openJoinedList');
export const closeJoinedList = createAction('closeJoinedList');

export const fetchCategories = createAction('fetchCategories');
export const openCategoriesMenu = createAction('openCategoriesMenu');
export const closeCategoriesMenu = createAction('closeCategoriesMenu');
export const setCurrentCategory = createAction('setCurrentCategory');
export const unsetCurrentCategory = createAction('unsetCurrentCategory');

export const openNewGroupDialog = createAction('openNewGroupDialog');
export const closeNewGroupDialog = createAction('closeNewGroupDialog');

export class DataStore extends Store {
  state : AppState = {
    categories: [],
    categoriesMenuAnchorEl: undefined,
    currentCategory: undefined,
    groups: [],
    groupDetails: undefined,
    isJoinedListOpen: false,
    isAddingNewGroup: false,
  };

  constructor() {
    super();
    this.listenTo(fetchGroups, this.fetchGroups);

    this.listenTo(fetchGroupDetails, this.fetchGroupDetails);
    this.listenTo(closeGroupDetails, this.closeGroupDetails);

    this.listenTo(openJoinedList, this.openJoinedList);
    this.listenTo(closeJoinedList, this.closeJoinedList);

    this.listenTo(fetchCategories, this.fetchCategories);
    this.listenTo(openCategoriesMenu, this.openCategoriesMenu);
    this.listenTo(closeCategoriesMenu, this.closeCategoriesMenu);
    this.listenTo(setCurrentCategory, this.setCurrentCategory);
    this.listenTo(unsetCurrentCategory, this.unsetCurrentCategory);

    this.listenTo(openNewGroupDialog, this.openNewGroupDialog);
    this.listenTo(closeNewGroupDialog, this.closeNewGroupDialog);
  }

  private fetchGroups = () => {
    fetch(`/api/groups/?email=${encodeURIComponent(localStorage.getItem("email") || "")}&token=${encodeURIComponent(localStorage.getItem("token") || "")}`, {
      credentials : 'same-origin',
      method : 'GET',
    }).then(response => {
      if (!response.ok) {
        return [];
      }

      return response.json();
    }).then(responseJson => {
      if (responseJson) {
        responseJson.forEach((group: BasicGroupDetails) => {
          group.start_time = new Date(Date.parse(group.start_time as string));
          group.end_time = new Date(Date.parse(group.end_time as string));
          group.created_at = new Date(Date.parse(group.created_at as string));
          group.updated_at = new Date(Date.parse(group.updated_at as string));
        });
      }

      this.setState({groups: responseJson});
    });
  }

  private fetchGroupDetails = (id: number) => {
    fetch(`/api/groups/${id}?email=${encodeURIComponent(localStorage.getItem("email") || "")}&token=${encodeURIComponent(localStorage.getItem("token") || "")}`, {
      credentials : 'same-origin',
      method : 'GET',
    }).then(response => {
      if (!response.ok) {
        if (response.status === 401) {
          localStorage.removeItem('token');
          window.location.href = "/login";
        }

        return undefined;
      }

      return response.json();
    }).then(responseJson => {
      if (responseJson) {
        responseJson.start_time = new Date(Date.parse(responseJson.start_time));
        responseJson.end_time = new Date(Date.parse(responseJson.end_time));
        responseJson.created_at = new Date(Date.parse(responseJson.created_at));
        responseJson.updated_at = new Date(Date.parse(responseJson.updated_at));
      }

      this.setState({groupDetails: responseJson});
    });
  }

  private closeGroupDetails = () => this.setState({groupDetails : undefined});

  private openJoinedList = () => this.setState({isJoinedListOpen : true});
  private closeJoinedList = () => this.setState({isJoinedListOpen : false});

  private openCategoriesMenu = (categoriesMenuAnchorEl: HTMLElement) => this.setState({categoriesMenuAnchorEl, currentCategory : undefined});
  private closeCategoriesMenu = () => this.setState({categoriesMenuAnchorEl : undefined, currentCategory : undefined});

  private fetchCategories = () => {
    fetch(`/api/activity_categories/?email=${encodeURIComponent(localStorage.getItem("email") || "")}&token=${encodeURIComponent(localStorage.getItem("token") || "")}`, {
      credentials : 'same-origin',
      method : 'GET',
    }).then(response => {
      if (!response.ok) {
        if (response.status === 401) {
          localStorage.removeItem('token');
          window.location.href = "/login";
        }

        return [];
      }

      return response.json();
    }).then(responseJson => {
      if (responseJson) {
        responseJson.forEach((category: Category) => {
          category.created_at = new Date(Date.parse(category.created_at as string));
          category.updated_at = new Date(Date.parse(category.updated_at as string));
        });
      }

      this.setState({categories: responseJson});
    });
  }

  private setCurrentCategory = (id: number) => {
    const currentCategory = this.state.categories.find(category => category.id === id);
    this.setState({currentCategory, categoriesMenuAnchorEl : undefined});
  }

  private unsetCurrentCategory = () => this.setState({currentCategory : undefined});

  private openNewGroupDialog = () => this.setState({isAddingNewGroup: true});
  private closeNewGroupDialog = () => this.setState({isAddingNewGroup: false});
}