import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import * as React from 'react';
import * as Reflux from 'reflux';
import './GroupDetails.css';
import { DataStore, closeGroupDetails, openJoinedList } from 'src/State';

interface Activity {
  id: number;
  name_en: string;
  category_id: number;
  created_at: Date;
  updated_at: Date;
}

interface Soldier {
  id: number;
  first_name: string;
  last_name: string;
  identity_code: number;
  rank_id: number;
  superior_id: Soldier;
  created_at: Date;
  updated_at: Date;
}

interface FullGroupData {
  id: number;
  start_time: Date;
  end_time: Date;
  max_participants: number;
  location: string;
  created_at: Date;
  updated_at: Date;
  activity: Activity;
  soldiers: Soldier[];
}

interface GroupDetailsState {
  groupDetails?: FullGroupData;
}

export class GroupDetails extends Reflux.Component {
  state: GroupDetailsState = {
    groupDetails: undefined,
  };

  constructor(props : {}) {
    super(props);
    this.store = DataStore;
    this.storeKeys = ['groupDetails'];
  }

  render() {
    const group = this.state.groupDetails;

    return (
      <Dialog
        open={Boolean(group)}
        onClose={closeGroupDetails}
      >
        <div className="group_details">
          <DialogTitle id='group_details'>Group Details</DialogTitle>
          <DialogContent>
            <DialogContentText>
              <u>Activity</u>: {group ? group.activity.name_en : ""}<br />
              <u>Time</u>: {group ? `${this.formatNumber(group.start_time.getHours())}:${this.formatNumber(group.start_time.getMinutes())} - ${this.formatNumber(group.end_time.getHours())}:${this.formatNumber(group.end_time.getMinutes())}` : ""}<br />
              <u>Location</u>: {group ? group.location : ""}<br />
              <u>Joined</u>: {group ? `${group.soldiers.length}/${group.max_participants}` : ""}
            </DialogContentText>
            <Button onClick={openJoinedList} color='secondary' variant="outlined">
              List
            </Button>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.joinGroup} color='primary'>
              {Boolean(group ? group.soldiers.find(soldier => soldier.id === Number(localStorage.getItem("soldierId"))) : false) ? "Leave" : "Join"}
            </Button>
            <Button onClick={closeGroupDetails} color='secondary'>
              Close
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    );
  }

  private joinGroup = () => {
    
  }

  private formatNumber(value: number) : string {
    if (value < 10) {
      return `0${value}`;
    }

    return `${value}`;
  }
}