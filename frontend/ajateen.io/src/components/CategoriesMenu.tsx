import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Popover from '@material-ui/core/Popover';
import * as React from 'react';
import * as Reflux from 'reflux';
import './CategoriesMenu.css';
import { DataStore, closeCategoriesMenu, setCurrentCategory } from 'src/State';

interface Category {
  id: number;
  name_en: string;
  created_at: string | Date;
  updated_at: string | Date;
}

interface CategoriesMenuState {
  categories: Category[];
  categoriesMenuAnchorEl?: HTMLElement;
}

export class CategoriesMenu extends Reflux.Component {
  state: CategoriesMenuState = {
    categories: [],
    categoriesMenuAnchorEl: undefined,
  };

  constructor(props : {}) {
    super(props);
    this.store = DataStore;
    this.storeKeys = ['categories', 'categoriesMenuAnchorEl'];
  }

  render() {
    const isOpen = Boolean(this.state.categoriesMenuAnchorEl);
    const categoriesList = [];
    if (isOpen) {
      for (let i = 0; i < this.state.categories.length; i++) {
        if (i > 0) {
          categoriesList.push(<Divider />);
        }

        categoriesList.push(
          <Button id={this.state.categories[i].id.toString(10)} onClick={this.onCategorySelect} fullWidth={true}>
            {this.state.categories[i].name_en}
          </Button>
        );
      }
    }

    return (
      <Popover 
        onClose={closeCategoriesMenu}
        open={isOpen}
        anchorEl={this.state.categoriesMenuAnchorEl}
        anchorOrigin={{
          horizontal: 'right',
          vertical: 'bottom',
        }}
        transformOrigin={{
          horizontal: 'right',
          vertical: 'top',
        }}
      >
        <div>
          {categoriesList}
        </div>
      </Popover>
    );
  }

  private onCategorySelect = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setCurrentCategory(Number(event.currentTarget.id));
  }
}
