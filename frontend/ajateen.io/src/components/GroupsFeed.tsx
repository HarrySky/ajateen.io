import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import * as React from 'react';
import * as Reflux from 'reflux';
import './GroupsFeed.css';
import { GroupCard } from './GroupCard';
import { DataStore, fetchGroups, fetchGroupDetails, fetchCategories } from 'src/State';

interface BasicGroupDetails {
  id: number;
  title: string;
  activity_id: number;
  participants: number;
  max_participants: number;
  start_time: Date;
  end_time: Date;
  location: string;
  created_at: Date;
  updated_at: Date;
}

interface DateWithGroups {
  date: Date;
  groups: BasicGroupDetails[];
}

interface GroupdsFeedState {
  groups: BasicGroupDetails[];
}

export class GroupsFeed extends Reflux.Component {
  state: GroupdsFeedState = {
    groups: [],
  };

  constructor(props : {}) {
    super(props);
    this.store = DataStore;
    this.storeKeys = ['groups'];
  }

  render() {
    const groups = this.state.groups;
    const datesWithGroups: DateWithGroups[] = [];

    if (groups.length === 0) {
      return (<h3 className="nothing">NOTHING TO DO :C</h3>);
    }

    groups.sort((a, b) => a.start_time.valueOf() - b.start_time.valueOf());

    let currentDate = groups[0].start_time;
    let groupsForDate: BasicGroupDetails[] = [];
    
    groups.forEach((group, index) => {
      if (group.start_time.toDateString() !== currentDate.toDateString()) {
        datesWithGroups.push({date: currentDate, groups: groupsForDate});
        currentDate = group.start_time;
        groupsForDate = [];
      }

      groupsForDate.push(group);

      if (index+1 === groups.length) {
        datesWithGroups.push({date: currentDate, groups: groupsForDate});
      }
    });

    const today = new Date();
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    const todayDate = today.toDateString();
    const tomorrowDate = tomorrow.toDateString();

    return (
      <GridList cellHeight={120} spacing={0} className="groups_feed">
        {datesWithGroups.map(dateWithGroup => {
          const dateElements = [];
          const date = dateWithGroup.date;
          dateElements.push(
            <GridListTile key="date" cols={2} style={{ height: 'auto' }}>
              <ListSubheader className="groups_date" component="div">{`${this.formatNumber(date.getDate())}.${this.formatNumber(date.getMonth()+1)} (${dateWithGroup.groups.length})${todayDate === date.toDateString() ? " - TODAY" : tomorrowDate === date.toDateString() ? " - TOMORROW" : ""}`}</ListSubheader>
            </GridListTile>
          );

          dateElements.push(
            dateWithGroup.groups.map(group => (
              <GridListTile id={group.id.toString(10)} key={group.id} className="group_tile" cols={1} rows={1} onClick={this.showGroupDetails}>
                <GridListTileBar
                  title={group.title}
                  titlePosition="top"
                  className="group_title"
                />
                <GroupCard group={group}/>
              </GridListTile>
            ))
          );
          
          return dateElements;
        })
      }
      </GridList>
    );
  }

  componentDidMount() {
    fetchGroups();
    fetchCategories();
  }

  private showGroupDetails = (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => fetchGroupDetails(event.currentTarget.id);

  private formatNumber(value: number) : string {
    if (value < 10) {
      return `0${value}`;
    }

    return `${value}`;
  }
}