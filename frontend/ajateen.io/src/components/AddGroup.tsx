import Fab from '@material-ui/core/Fab';
import Zoom from '@material-ui/core/Zoom';
import AddIcon from '@material-ui/icons/Add';
import * as React from 'react';
import * as Reflux from 'reflux';
import './AddGroup.css';
import { openNewGroupDialog } from 'src/State';

export class AddGroup extends Reflux.Component {
  render() {
    return (
      <Zoom
        key="add_group"
        in={true}
        timeout={1000}
        unmountOnExit={true}
      >
        <Fab aria-label="add_group" className="add_group" color="primary" onClick={openNewGroupDialog}>
          <AddIcon />
        </Fab>
      </Zoom>
    );
  }
}
